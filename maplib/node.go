package maplib

import "fmt"

/*NodeType used for enum*/
type NodeType int

/*Constant for the type of Node*/
const (
	Path NodeType = iota
	Block
)

/*Node struct is a single node in the A-star map*/
type Node struct {
	FCost, GCost, HCost float64
	Neighbor            map[int]*Node
	Parent              *Node
	Type                NodeType
	Name                string
	Coords              []int // X = 0, Y = 1, Z = 2, etc.
}

/*Coordinates is a method of Node struct that prints the Node's coordinates*/
func (n Node) Coordinates() {
	fmt.Printf("Name: %v (%d, %d)\n", n.Name, n.Coords[0], n.Coords[1])
}

/*IsPath returns true if a Node is a Path*/
func (n Node) IsPath() (isPath bool) {
	isPath = false
	if n.Type == Path {
		isPath = true
	}
	return
}
