package maplib

import (
	"fmt"
	"math"
	"strconv"
)

const numOf3Dimensions int = 3

/*Constant for the different directions of the Node's neighbor*/
const (
	z int = iota + 8
	xz
	xz1
	z1
	x1z1
	x1z

	yz
	yz1
	y1z1
	y1z

	xyz
	xy1z
	x1y1z1
	x1yz1

	x1yz
	xyz1
	xy1z1
	x1y1z
)

/*Map3D single dimension array of nodes*/
type Map3D struct {
	content []Node
	size    int
}

/*Size returns the size of the map*/
func (m *Map3D) Size() int {
	return m.size
}

/*GetNode gets a node based on the coordinates provided*/
func (m *Map3D) GetNode(coord ...interface{}) *Node {
	var node *Node
	if assertCount(numOf3Dimensions, coord...) {
		x, ok1 := coord[0].(int)
		y, ok2 := coord[1].(int)
		z, ok3 := coord[2].(int)

		if ok1 && ok2 && ok3 {
			width := m.width()
			height := m.height()

			//TODO: Check for out of bounds
			node = &m.content[(y*width+x)+(width*height*z)]
		}

	} else {
		fmt.Println("Map3D: Incorrect number of coordinates provided")
	}
	return node
}

/*Create creates a map based on the row, column and height count.*/
func (m *Map3D) Create(num ...interface{}) {
	//assert that the num is only 3
	if assertCount(numOf3Dimensions, num...) {

		xCount, ok1 := num[0].(int)
		yCount, ok2 := num[1].(int)
		zCount, ok3 := num[2].(int)

		if ok1 && ok2 && ok3 {
			maxCount := xCount * yCount * zCount
			maxCountPerLevel := xCount * yCount

			m.content = make([]Node, maxCount)
			m.size = maxCount

			for i := 0; i < maxCount; i++ {

				xCtr := i % xCount
				zCtr := int(i / maxCountPerLevel)
				yCtr := int((i - (zCtr * maxCountPerLevel)) / xCount)
				m.content[i] = Node{Name: strconv.Itoa(i), Coords: []int{xCtr, yCtr, zCtr}, Neighbor: make(map[int]*Node)}

				if xCtr > 0 {
					m.content[i].Neighbor[x1] = &m.content[i-1]
					m.content[i-1].Neighbor[x] = &m.content[i]
				}

				if yCtr > 0 {
					m.content[i].Neighbor[y] = &m.content[i-xCount]
					m.content[i-xCount].Neighbor[y1] = &m.content[i]
				}

				if xCtr > 0 && yCtr > 0 {
					m.content[i].Neighbor[x1y] = &m.content[i-xCount-1]
					m.content[i-xCount-1].Neighbor[xy1] = &m.content[i]
				}

				if xCtr >= 0 && yCtr > 0 && xCtr < xCount-1 {
					m.content[i].Neighbor[xy] = &m.content[i-(xCount-1)]
					m.content[i-(xCount-1)].Neighbor[x1y1] = &m.content[i]
				}

				if zCtr > 0 {
					m.content[i].Neighbor[z1] = &m.content[i-maxCountPerLevel]
					m.content[i-maxCountPerLevel].Neighbor[z] = &m.content[i]
				}

				if zCtr > 0 && xCtr > 0 {
					m.content[i].Neighbor[x1z1] = &m.content[i-maxCountPerLevel-1]
					m.content[i-maxCountPerLevel-1].Neighbor[xz] = &m.content[i]
				}

				if zCtr > 0 && yCtr > 0 {
					m.content[i].Neighbor[yz1] = &m.content[i-maxCountPerLevel-xCount]
					m.content[i-maxCountPerLevel-xCount].Neighbor[y1z] = &m.content[i]
				}

				if zCtr > 0 && xCtr > 0 && yCtr > 0 {
					m.content[i].Neighbor[x1yz1] = &m.content[i-maxCountPerLevel-xCount-1]
					m.content[i-maxCountPerLevel-xCount-1].Neighbor[xy1z] = &m.content[i]
				}

				if zCtr > 0 && xCtr >= 0 && yCtr > 0 && xCtr < xCount-1 {
					m.content[i].Neighbor[xyz1] = &m.content[i-maxCountPerLevel-(xCount-1)]
					m.content[i-maxCountPerLevel-(xCount-1)].Neighbor[x1y1z] = &m.content[i]
				}

				if zCtr > 0 && xCtr >= 0 && xCtr < xCount-1 {
					m.content[i].Neighbor[xz1] = &m.content[i-maxCountPerLevel+1]
					m.content[i-maxCountPerLevel+1].Neighbor[x1z] = &m.content[i]
				}

				if zCtr > 0 && yCtr < yCount-1 {
					m.content[i].Neighbor[y1z1] = &m.content[i-maxCountPerLevel+xCount]
					m.content[i-maxCountPerLevel+xCount].Neighbor[yz] = &m.content[i]
				}

				if zCtr > 0 && xCtr > 0 && yCtr < yCount-1 {
					m.content[i].Neighbor[x1y1z1] = &m.content[i-maxCountPerLevel+(xCount-1)]
					m.content[i-maxCountPerLevel+(xCount-1)].Neighbor[xyz] = &m.content[i]
				}

				if zCtr > 0 && yCtr < yCount-1 && xCtr < xCount-1 {
					m.content[i].Neighbor[xy1z1] = &m.content[i-maxCountPerLevel+xCount+1]
					m.content[i-maxCountPerLevel+xCount+1].Neighbor[x1yz] = &m.content[i]
				}

			}
		}

	} else {
		fmt.Println("Failed to create 3D Map: Incorrect number of parameters provided")
	}
}

/*Print prints the map*/
func (m *Map3D) Print() {
	for i := 0; i < m.size; i++ {
		fmt.Printf("%p: %v\n", &m.content[i], m.content[i])
	}
}

/*CalculateHeuristicDistance computes the heuristic distance between 2 Node structs*/
func (m *Map3D) CalculateHeuristicDistance(node ...interface{}) float64 {

	if assertCount(2, node...) {

		//The easiest calculation for a heuristic is the distance formula. But this can be any heuristic function.
		return m.CalculateDistance(node...)
	}

	fmt.Println("Map3D: Failed to calculate heuristic distance. Incorrect number of parameters provided.")
	return 0.0

}

/*CalculateDistance computes the distance between 2 Node structs*/
func (m *Map3D) CalculateDistance(node ...interface{}) float64 {

	if assertCount(2, node...) {

		start, ok1 := node[0].(Node)
		goal, ok2 := node[1].(Node)

		if ok1 && ok2 {
			xsqr := float64((goal.Coords[0] - start.Coords[0]) * (goal.Coords[0] - start.Coords[0]))
			ysqr := float64((goal.Coords[1] - start.Coords[1]) * (goal.Coords[1] - start.Coords[1]))
			zsqr := float64((goal.Coords[2] - start.Coords[2]) * (goal.Coords[2] - start.Coords[2]))
			return math.Sqrt(xsqr + ysqr + zsqr)
		}

	}

	fmt.Println("Map3D: Failed to calculate distance. Incorrect number of parameters provided.")
	return 0.0

}

func (m *Map3D) width() int {
	currNode := &m.content[0]
	width := 0

	for currNode != nil {
		width++
		currNode = currNode.Neighbor[x]
	}
	return width
}

func (m *Map3D) height() int {
	currNode := &m.content[0]
	height := 0

	for currNode != nil {
		height++
		currNode = currNode.Neighbor[y1]
	}
	return height
}

func (m *Map3D) depth() int {
	currNode := &m.content[0]
	depth := 0

	for currNode != nil {
		depth++
		currNode = currNode.Neighbor[z]
	}
	return depth
}
