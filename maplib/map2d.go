package maplib

import (
	"fmt"
	"math"
	"strconv"
)

var numOf2Dimensions = 2

const (
	y int = iota
	xy
	x
	xy1
	y1
	x1y1
	x1
	x1y
)

/*Map2D single dimension array of nodes*/
type Map2D struct {
	content []Node
	size    int
}

/*Size returns the size of the map*/
func (m *Map2D) Size() int {
	return m.size
}

/*GetNode gets a node based on the coordinates provided*/
func (m *Map2D) GetNode(coord ...interface{}) *Node {
	var node *Node
	if assertCount(numOf2Dimensions, coord...) {
		x, ok1 := coord[0].(int)
		y, ok2 := coord[1].(int)

		if ok1 && ok2 {
			width := m.width()
			//TODO: Bound check here
			node = &m.content[(y*width)+x]
		}

	} else {
		fmt.Println("Map2D: Incorrect number of coordinates provided")
	}
	return node
}

/*Create creates a map based on the row and column count.*/
func (m *Map2D) Create(num ...interface{}) {
	//assert that the num is only 2
	if assertCount(numOf2Dimensions, num...) {

		rowCount, ok1 := num[0].(int)
		colCount, ok2 := num[1].(int)

		if ok1 && ok2 {
			maxCount := rowCount * colCount

			m.content = make([]Node, maxCount)
			m.size = maxCount

			for i := 0; i < maxCount; i++ {
				rowCtr := i % rowCount
				colCtr := int(i / rowCount)

				m.content[i] = Node{Name: strconv.Itoa(i), Coords: []int{rowCtr, colCtr}, Neighbor: make(map[int]*Node)}

				if rowCtr > 0 {
					m.content[i].Neighbor[x1] = &m.content[i-1]
					m.content[i-1].Neighbor[x] = &m.content[i]
				}

				if colCtr > 0 {
					m.content[i].Neighbor[y] = &m.content[i-rowCount]
					m.content[i-rowCount].Neighbor[y1] = &m.content[i]
				}

				if rowCtr > 0 && colCtr > 0 {
					m.content[i].Neighbor[x1y] = &m.content[i-rowCount-1]
					m.content[i-rowCount-1].Neighbor[xy1] = &m.content[i]
				}

				if rowCtr >= 0 && colCtr > 0 && rowCtr < rowCount-1 {
					m.content[i].Neighbor[xy] = &m.content[i-(rowCount-1)]
					m.content[i-(rowCount-1)].Neighbor[x1y1] = &m.content[i]
				}
			}
		}

	} else {
		fmt.Println("Failed to create 2D Map: Incorrect number of parameters provided")
	}
}

/*Print prints the map*/
func (m *Map2D) Print() {
	for i := 0; i < m.size; i++ {
		fmt.Printf("%p: %v\n", &m.content[i], m.content[i])
	}
}

/*CalculateHeuristicDistance computes the heuristic distance between 2 Node structs*/
func (m *Map2D) CalculateHeuristicDistance(node ...interface{}) float64 {

	if assertCount(numOf2Dimensions, node...) {

		//The easiest calculation for a heuristic is the distance formula. But this can be any heuristic function.
		return m.CalculateDistance(node...)
	}

	fmt.Println("Map2d: Failed to calculate heuristic distance. Incorrect number of parameters provided.")
	return 0.0

}

/*CalculateDistance computes the distance between 2 Node structs*/
func (m *Map2D) CalculateDistance(node ...interface{}) float64 {

	if assertCount(numOf2Dimensions, node...) {

		start, ok1 := node[0].(Node)
		goal, ok2 := node[1].(Node)

		if ok1 && ok2 {
			xsqr := float64((goal.Coords[0] - start.Coords[0]) * (goal.Coords[0] - start.Coords[0]))
			ysqr := float64((goal.Coords[1] - start.Coords[1]) * (goal.Coords[1] - start.Coords[1]))
			return math.Sqrt(xsqr + ysqr)
		}

	}

	fmt.Println("Map2d: Failed to calculate distance. Incorrect number of parameters provided.")
	return 0.0

}

func (m *Map2D) width() int {
	currNode := &m.content[0]
	width := 0

	for currNode != nil {
		width++
		currNode = currNode.Neighbor[x]
	}
	return width
}
