package maplib

import "fmt"

/*NodeSet is a slice of pointer to Node structs*/
type NodeSet []*Node

/*LowestCostNodeInSet retrieves the Node that has the lowest F cost in the set provided*/
func (s *NodeSet) LowestCostNodeInSet() (lowest *Node) {

	for _, n := range *s {
		if lowest == nil || n.FCost < lowest.FCost {
			lowest = n
		}
	}

	return
}

/*RemoveNodeInSet removes the target Node from the set provided*/
func (s *NodeSet) RemoveNodeInSet(target *Node) {
	for i, n := range *s {
		if n == target {
			*s = append((*s)[:i], (*s)[i+1:]...)
			break
		}
	}
}

/*CheckIfNodeExists checks if a Node is already in the set.*/
func (s *NodeSet) CheckIfNodeExists(target *Node) (exists bool) {
	for _, n := range *s {
		if n == target {
			exists = true
			break
		}
	}
	return
}

/*String prints the content of the NodeSet*/
func (s *NodeSet) String() string {

	var print string

	for _, n := range *s {
		print += fmt.Sprintf("%p: %v ", n, n)
	}

	return print

}
