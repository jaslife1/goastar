package maplib

/*IMap interface for different type of Maps (e.g. 1D array, 2D array, 3D array, etc.*/
type IMap interface {
	Size() int
	GetNode(coord ...interface{}) *Node
	Create(num ...interface{})
	Print()
	CalculateHeuristicDistance(node ...interface{}) float64
	CalculateDistance(node ...interface{}) float64
}
