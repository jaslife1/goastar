package maplib

/*MapType is used for enum*/
type MapType int

/*Enum for map types*/
const (
	TwoDimension MapType = iota
	ThreeDimension
)

/*CreateMap is a factory for creating the map*/
func CreateMap(mapType MapType, num ...interface{}) IMap {
	switch mapType {
	case TwoDimension:
		imap := Map2D{}
		imap.Create(num...)

		//Return the address of the concrete object when returning an interface
		return &imap
	case ThreeDimension:
		imap := Map3D{}
		imap.Create(num...)

		return &imap
	default:

	}

	return nil
}
