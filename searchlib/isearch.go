package searchlib

import (
	"gitlab.com/jaslife1/goastar/maplib"
)

type node = maplib.Node
type iMap = maplib.IMap
type nodeSet = maplib.NodeSet

/*ISearch interface for searching a node in a map of nodes*/
type ISearch interface {
	Search(start, goal *node, graph iMap) (nodeSet, bool)
}
