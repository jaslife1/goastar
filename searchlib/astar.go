package searchlib

/*AStar struct implements the ISearch*/
type AStar struct {
}

/*Search searches the goal Node from a start Node in a Map*/
func (a *AStar) Search(start, goal *node, graph iMap) (nodeSet, bool) {
	openSet := make(nodeSet, 0, graph.Size())
	closedSet := make(nodeSet, 0, graph.Size())
	goalSet := make(nodeSet, 0)

	openSet = append(openSet, start)
	//calculate the total cost. The start node will have 0 for GCost and only HCost (heuristic cost)
	start.FCost = graph.CalculateHeuristicDistance(*start, *goal)

	goalFound := false
	for len(openSet) > 0 {
		current := openSet.LowestCostNodeInSet()
		if current == goal {
			goalFound = true
			tmp := current
			for tmp != nil {
				goalSet = append(goalSet, tmp)
				tmp = tmp.Parent
			}

			break
		}
		closedSet = append(closedSet, current)
		openSet.RemoveNodeInSet(current)

		for _, value := range current.Neighbor {

			if !value.IsPath() || closedSet.CheckIfNodeExists(value) || value == nil {
				continue
			}

			gCost := current.GCost + graph.CalculateDistance(*current, *value)

			if !openSet.CheckIfNodeExists(value) {
				openSet = append(openSet, value)
			} else if gCost >= value.GCost {
				continue
			}

			// So far this is the best path - record it
			value.Parent = current
			value.GCost = gCost
			value.HCost = graph.CalculateHeuristicDistance(*value, *goal)
			value.FCost = value.GCost + value.HCost

		}
	}

	return goalSet, goalFound
}
