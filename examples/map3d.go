package examples

import (
	"fmt"
	"time"

	"gitlab.com/jaslife1/goastar/maplib"
	"gitlab.com/jaslife1/goastar/searchlib"
)

func Test3D() {
	startTime := time.Now()
	//create a map - 5 x 3 x 3
	rowCount, colCount, heightCount := 5, 3, 3
	sampleMap := maplib.CreateMap(maplib.ThreeDimension, rowCount, colCount, heightCount)

	start := sampleMap.GetNode(0, 1, 0)
	goal := sampleMap.GetNode(4, 0, 2)

	search := searchlib.AStar{}
	nodes, _ := search.Search(start, goal, sampleMap)
	for _, tmp := range nodes {
		fmt.Printf("{%v}\n", tmp.Coords)
	}
	fmt.Println("Time since: ", time.Since(startTime))
}
