package examples

import (
	"fmt"
	"time"

	"gitlab.com/jaslife1/goastar/maplib"
	"gitlab.com/jaslife1/goastar/searchlib"
)

func Test2D() {
	startTime := time.Now()
	//create a map - 5 x 3
	rowCount, colCount := 5, 3
	sampleMap := maplib.CreateMap(maplib.TwoDimension, rowCount, colCount)

	start := sampleMap.GetNode(0, 1)
	goal := sampleMap.GetNode(4, 0)

	search := searchlib.AStar{}
	nodes, _ := search.Search(start, goal, sampleMap)
	for _, tmp := range nodes {
		fmt.Printf("{%v}\n", tmp.Coords)
	}
	fmt.Println("Time since: ", time.Since(startTime))
}
