# GoAstar

This is an A-Star implementation using Go.

A-Star finds the shortest distance between two nodes in a map. The implementation of this search algorithm is flexible to cater to multidimensional maps (e.g. 2D, 3D, etc.).

There is currently 2 types of map - a 2D and 3D map.

## 2D Map

    rowCount, colCount := 5, 3
	sampleMap := maplib.CreateMap(maplib.TwoDimension, rowCount, colCount)

	start := sampleMap.GetNode(0, 1)
	goal := sampleMap.GetNode(4, 0)

	search := searchlib.AStar{}
	nodes, found := search.Search(start, goal, sampleMap)


## 3D Map

    rowCount, colCount, heightCount := 5, 3, 3
	sampleMap := maplib.CreateMap(maplib.ThreeDimension, rowCount, colCount, heightCount)

	start := sampleMap.GetNode(0, 1, 0)
	goal := sampleMap.GetNode(4, 0, 2)

	search := searchlib.AStar{}
	nodes, found := search.Search(start, goal, sampleMap)